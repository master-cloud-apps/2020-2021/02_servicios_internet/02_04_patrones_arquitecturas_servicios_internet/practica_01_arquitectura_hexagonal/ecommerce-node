const shoppingCart = require('../../../src/domain/shoppingcart/usecase.js')
const { expect } = require('chai')
const shoppingCartRepository = require('../../infraestructure/db/shoppingCartRepository.js')
const productRepository = require('../../infraestructure/db/productRepository.js')
const { getTestData } = require('./index.js')
const shoppingCartService = require('../../infraestructure/services/shoppingcart.js')
const { exampleId } = require('../../endpoints/products/index.js')
const shoppingCartUseCase = shoppingCart({ shoppingCartRepository, productRepository, shoppingCartService })

describe('Shopping Cart domain', () => {
  describe('End Cart examples', () => {
    let carts
    beforeEach(() => {
      return getTestData().then(result => {
        carts = result.carts
      })
    })
    it('Given cart with one product, when end product should return OK', () => {
      return shoppingCartUseCase.endCart({ id: carts[1].id })
        .then(cart => expect(cart.status).to.be.equal('Completed'))
    })
    it('Given cart with two products, when end product should return error', () => {
      return shoppingCartUseCase.endCart({ id: carts[0].id })
        .then(
          () => Promise.reject(new Error('Expect method to reject.')),
          error => {
            expect(error.errors).not.to.be.equal(undefined)
            expect(error.errors.message).to.be.equal('Some products are not available')
          })
    })
  })
  describe('End cart errors', () => {
    it('Given not existing cart, when end cart should return not found', () => {
      return shoppingCartUseCase.endCart({ id: exampleId })
        .then(() => Promise.reject(new Error('Expect method to reject.')),
          error => {
            expect(error.errors).not.to.be.equal(undefined)
            expect(error.errors.message).to.be.equal('ShoppingCart not found')
          })
    })
  })
})
