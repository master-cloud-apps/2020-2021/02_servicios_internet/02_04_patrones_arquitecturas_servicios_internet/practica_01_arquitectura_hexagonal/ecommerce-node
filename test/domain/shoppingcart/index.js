const shoppingCartRepository = require('../../infraestructure/db/shoppingCartRepository.js')
const productRepository = require('../../infraestructure/db/productRepository.js')
const shoppingCart = require('../../../src/domain/shoppingcart/usecase.js')
const shoppingCartUseCase = shoppingCart({ shoppingCartRepository, productRepository })

const testCarts = [{ status: 'Created', cartItems: [] }, { status: 'Pending', cartItems: [] }]
const testProducts = [{
  kind: 'Book',
  name: 'Love in the Time of Cholera',
  description: 'A secret relationship blossoms between the two with the help of Ferminas Aunt Escolástica'
}, {
  kind: 'Board',
  name: 'Samsung Board',
  description: 'Classic Board for computers'
}]

const getTestProducts = () => {
  return Promise.all(testProducts.map(product => productRepository.save(product)))
}

const getTestData = () => {
  return getTestProducts().then(products => {
    return Promise.all(testCarts.map(cart => shoppingCartRepository.save(cart)))
      .then(cartsSaved => {
        const cart1 = cartsSaved[0]
        const cart2 = cartsSaved[1]
        return shoppingCartUseCase.addProductToShoppingCart({ cartId: cart1.id, productId: products[0].id, quantity: 10 })
          .then(() => shoppingCartUseCase.addProductToShoppingCart({ cartId: cart1.id, productId: products[1].id, quantity: 10 }))
          .then(() => shoppingCartUseCase.addProductToShoppingCart({ cartId: cart2.id, productId: products[1].id, quantity: 10 }))
          .then(() => ({ carts: cartsSaved, products }))
      })
  })
}

module.exports = {
  getTestProducts,
  getTestData
}
