const shoppingCart = require('../../../src/domain/shoppingcart/usecase.js')
const { expect } = require('chai')
const shoppingCartRepository = require('../../infraestructure/db/shoppingCartRepository.js')
const productRepository = require('../../infraestructure/db/productRepository.js')
const { exampleShoppingCart } = require('../index.js')

describe('Shopping Cart domain', () => {
  const shoppingCartUseCase = shoppingCart({ shoppingCartRepository, productRepository })
  describe('Create shoppingCart examples', () => {
    it('Should create a shopping cart', () => {
      return shoppingCartUseCase
        .createShoppingCart({ shoppingCart: exampleShoppingCart })
        .then((finalCart) => expect(finalCart.id).to.not.be.equal(undefined))
    })
    it('When shopping cart created, cart can be found', () => {
      return shoppingCartUseCase.createShoppingCart({ shoppingCart: exampleShoppingCart })
        .then(({ id }) => shoppingCartUseCase.getShoppingCart({ id }))
        .then(cart => expect(cart.status).to.be.equal(exampleShoppingCart.status))
    })
    it('When shopping cart created with status Pending, cart can be found', () => {
      return shoppingCartUseCase.createShoppingCart({ shoppingCart: { status: 'Pending' } })
        .then(({ id }) => shoppingCartUseCase.getShoppingCart({ id }))
        .then(cart => {
          expect(cart.status).to.be.equal('Pending')
          expect(cart.cartItems).to.be.an('Array')
        })
    })
  })
})
