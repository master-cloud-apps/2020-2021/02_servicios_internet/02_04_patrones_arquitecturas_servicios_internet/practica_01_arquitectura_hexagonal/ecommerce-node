const exampleProduct = {
  kind: 'Book',
  name: 'Love in the Time of Cholera',
  description: 'A secret relationship blossoms between the two with the help of Ferminas Aunt Escolástica'
}
const exampleShoppingCart = {
  status: 'Created'
}

module.exports = {
  exampleProduct,
  exampleShoppingCart
}
