const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart } = require('../../domain/index.js')
const { getIdFromResponseLocationHeader } = require('../index.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  describe(`GET ${SHOPPING_CARTS_URI}/:cartId`, () => {
    manageInMemoryDatabase()
    it('Given shopping cart created, should be able to retrieve it by id', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)

      const cartId = getIdFromResponseLocationHeader(responseCartCreated)

      const cartGetResponse = await request(app).get(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(cartGetResponse.status).to.be.equal(200)
      expect(cartGetResponse.body.data.status).to.be.equal(exampleShoppingCart.status)
      expect(cartGetResponse.body.data.id).to.not.be.equal(undefined)
      expect(cartGetResponse.body.data.cartItems).to.be.an('array').that.has.lengthOf(0)
    })
    it('Given id wrong, should return bad request', async () => {
      const productResponse = await request(app).get(`${SHOPPING_CARTS_URI}/1`)

      expect(productResponse.status).to.be.equal(400)
      expect(productResponse.body.error.id.name).to.be.equal('ValidatorError')
    })
    it('Given id not found, should return not found', async () => {
      const productResponse = await request(app).get(`${SHOPPING_CARTS_URI}/6033994bc8baf639a235755f`)

      expect(productResponse.status).to.be.equal(404)
    })
  })
})
