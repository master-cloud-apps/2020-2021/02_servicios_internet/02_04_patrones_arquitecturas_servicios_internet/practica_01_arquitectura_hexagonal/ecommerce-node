const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { SHOPPING_CARTS_URI } = require('../../../src/app/endpoints/constants.js')
const { exampleShoppingCart } = require('../../domain/index.js')
const { getIdFromResponseLocationHeader } = require('../index.js')
const { exampleId } = require('../products/index.js')

const app = createApp(appDependencies)

describe('Endpoints for ShoppingCart', () => {
  describe(`DELETE ${SHOPPING_CARTS_URI}/:cartId`, () => {
    manageInMemoryDatabase()
    it('Given shopping cart, should be able to delete cart', async () => {
      const responseCartCreated = await request(app)
        .post(SHOPPING_CARTS_URI)
        .send(exampleShoppingCart)
      const cartId = getIdFromResponseLocationHeader(responseCartCreated)

      const deleteCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(deleteCartResponse.status).to.be.equal(204)

      const cartGetResponse = await request(app).get(`${SHOPPING_CARTS_URI}/${cartId}`)

      expect(cartGetResponse.status).to.be.equal(404)
    })
    it('When delete cart not existed should return not found', async () => {
      const deleteCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/${exampleId}`)

      expect(deleteCartResponse.status).to.be.equal(404)
    })
    it('When delete cart wrong cartId should return bad request', async () => {
      const deleteCartResponse = await request(app)
        .delete(`${SHOPPING_CARTS_URI}/1`)

      expect(deleteCartResponse.status).to.be.equal(400)
    })
  })
})
