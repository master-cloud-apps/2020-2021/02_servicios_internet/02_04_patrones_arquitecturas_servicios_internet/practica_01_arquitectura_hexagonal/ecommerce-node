const request = require('supertest')
const createApp = require('../../../src/app/index.js')
const appDependencies = require('../../../src/app/dependencies/index.js')
const { manageInMemoryDatabase } = require('../../db/index')
const { expect } = require('chai')
const { getIdFromResponseLocationHeader } = require('../index.js')
const { PRODUCTS_URI } = require('../../../src/app/endpoints/constants.js')

const app = createApp(appDependencies)

describe('Endpoints for Product', () => {
  manageInMemoryDatabase()
  describe(`GET ${PRODUCTS_URI}/:productId`, () => {
    it('Given book created, should be able to retrieve it by id', async () => {
      const responseProductCreated = await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', name: 'BookExample', description: 'Book Description' })

      const productId = getIdFromResponseLocationHeader(responseProductCreated)

      const productResponse = await request(app).get(`${PRODUCTS_URI}/${productId}`)

      expect(productResponse.status).to.be.equal(200)
      expect(productResponse.body.data.name).to.be.equal('BookExample')
    })
    it('Given id wrong, should return bad request', async () => {
      const productResponse = await request(app).get(`${PRODUCTS_URI}/1`)

      expect(productResponse.status).to.be.equal(400)
      expect(productResponse.body.error.id.name).to.be.equal('ValidatorError')
    })
    it('Given id not found, should return not found', async () => {
      const productResponse = await request(app).get(`${PRODUCTS_URI}/6033994bc8baf639a235755f`)

      expect(productResponse.status).to.be.equal(404)
    })
  })
  describe(`GET ${PRODUCTS_URI}`, () => {
    it('Given one product, get products should return ione element', async () => {
      await request(app)
        .post(PRODUCTS_URI)
        .send({ kind: 'Book', name: 'BookExample', description: 'Book Description' })
      const productsResponse = await request(app).get(`${PRODUCTS_URI}`)

      expect(productsResponse.status).to.be.equal(200)
      expect(productsResponse.body.data.length).to.be.equal(1)
    })
  })
})
