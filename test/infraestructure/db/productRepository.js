const products = []
const save = (product) => {
  const id = products.length
  products.push({ id, ...product })
  return Promise.resolve(findProductById(id))
}
const deleteProduct = (id) => {
  const indexToDelete = products.findIndex(product => product.id === id)
  const productToDelete = findProductById(id)
  products.splice(indexToDelete, 1)
  return Promise.resolve(productToDelete)
}
const getProduct = (id) => Promise.resolve(findProductById(id))
const findProductById = id => products.find(p => p.id === id)

module.exports = {
  save,
  getProduct,
  deleteProduct
}
