const isValid = cart => Promise.resolve(cart.cartItems.length % 2 !== 0)

module.exports = { isValid }
