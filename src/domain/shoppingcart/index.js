const _ = require('lodash')
const { createCartNotValidError } = require('../model')

const includeProductInShoppingCart = (cart, product, quantity) => {
  if (getIndexOfProduct(cart, product) === -1) {
    cart.cartItems.push({ quantity: quantity, product })
    return cart
  }
  cart.cartItems[getIndexOfProduct(cart, product)].quantity = quantity
  return cart
}
const getIndexOfProduct = (cart, product) => cart.cartItems.findIndex(item => item.product.id === product.id)

const validateCartOrThrow = (cart, isCartValid) => {
  const cartValidated = _.cloneDeep(cart)
  if (isCartValid) {
    cartValidated.status = 'Completed'
    return cartValidated
  }
  throw createCartNotValidError()
}

const deleteProductFromCart = (cart, productId) => {
  const cartWithoutProduct = _.cloneDeep(cart)
  cartWithoutProduct.cartItems = cartWithoutProduct.cartItems.filter(item => item.product.id !== productId)
  return cartWithoutProduct
}

module.exports = {
  includeProductInShoppingCart,
  validateCartOrThrow,
  deleteProductFromCart
}
