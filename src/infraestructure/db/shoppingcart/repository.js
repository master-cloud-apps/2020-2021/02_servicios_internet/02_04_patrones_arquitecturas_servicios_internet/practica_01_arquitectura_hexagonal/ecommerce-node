const { checkMongoId } = require('../../mongo')

module.exports = ({ ShoppingCart }) => {
  const createCartModel = cart => {
    if (cart.id === null || cart.id === undefined) {
      return new ShoppingCart(cart)
    }
    return ShoppingCart.findById(cart.id).then(cartFound => {
      cartFound.status = cart.status
      cartFound.cartItems = cart.cartItems.map(item => ({
        quantity: item.quantity,
        product: item.product.id
      }))
      return cartFound
    })
  }
  const save = (cart) =>
    Promise.resolve(createCartModel(cart))
      .then(model => model.save())
      .then(plainShoppingCart => plainShoppingCart.populate('cartItems.product').execPopulate())
      .then(modelSaved => modelSaved.toJSON())

  const getShoppingCart = (id) => {
    return Promise.resolve(checkMongoId(id))
      .then(cartId => ShoppingCart.findById(cartId).populate('cartItems.product'))
      .then(model => (model !== null ? model.toJSON() : null))
  }

  const deleteShoppingCart = (id) => {
    return Promise.resolve(checkMongoId(id))
      .then(cartId => ShoppingCart.findOneAndDelete({ _id: cartId }))
  }

  return {
    save,
    getShoppingCart,
    deleteShoppingCart
  }
}
