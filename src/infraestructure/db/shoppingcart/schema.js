const mongoose = require('mongoose')
const { removeMongooseElements } = require('../../mongo')

const shoppingCartSchema = new mongoose.Schema({
  status: { type: String, required: true },
  cartItems: [{
    quantity: { type: Number, required: true },
    product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' }
  }]
})

shoppingCartSchema.methods.toJSON = function () {
  const cartObject = removeMongooseElements(this.toObject())

  cartObject.cartItems = cartObject.cartItems.map(item => {
    const itemObject = removeMongooseElements(item)
    itemObject.product = removeMongooseElements(item.product)
    return itemObject
  })

  return cartObject
}

module.exports = mongoose.model('ShoppingCart', shoppingCartSchema)
