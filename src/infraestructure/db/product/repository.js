const { checkMongoId } = require('../../mongo')

module.exports = ({ Product }) => {
  const createProductModel = (product) => {
    return new Product(product)
  }
  const save = (product) =>
    Promise.resolve(createProductModel(product))
      .then((model) => model.save())
      .then((modelSaved) => modelSaved.toJSON())

  const getProduct = (id) => {
    return Promise.resolve(checkMongoId(id))
      .then(productId => Product.findById(productId))
      .then((model) => (model !== null ? model.toJSON() : null))
  }

  const getProducts = () => {
    return Product.find().then(products => products.map(product => product.toJSON()))
  }

  const deleteProduct = (id) => {
    return Promise.resolve(checkMongoId(id))
      .then(productId => Product.findOneAndDelete({ _id: productId }))
  }

  return {
    save,
    getProduct,
    getProducts,
    deleteProduct
  }
}
