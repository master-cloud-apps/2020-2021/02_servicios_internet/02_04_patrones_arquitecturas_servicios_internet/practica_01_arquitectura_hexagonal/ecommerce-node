const express = require('express')
const { PRODUCTS_URI } = require('../endpoints/constants.js')
const { products } = require('../endpoints/index.js')

module.exports = (useCases) => {
  const router = new express.Router()
  const productHandler = products(useCases)

  router.post(PRODUCTS_URI, productHandler.post)
  router.get(`${PRODUCTS_URI}/:productId`, productHandler.getById)
  router.delete(`${PRODUCTS_URI}/:productId`, productHandler.deleteById)
  router.get(PRODUCTS_URI, productHandler.get)

  return router
}
