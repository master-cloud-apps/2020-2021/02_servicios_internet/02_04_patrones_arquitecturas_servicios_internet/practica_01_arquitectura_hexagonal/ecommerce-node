const products = require('./products/index.js')
const shoppingCarts = require('./shoppingcarts/index.js')

module.exports = {
  products,
  shoppingCarts
}
