const { getFullUrl } = require('../request')
const { manageError, checkModelNone } = require('../response')

const handlers = ({ productUseCase }) => ({
  get: (req, res) => {
    return productUseCase.getProducts()
      .then(products => res.status(200).send({ data: products }))
  },
  post: (req, res) => {
    return productUseCase.createProduct(createProductFromRequest(req))
      .then(product => res.status(201).location(getFullUrl(req) + product.id).send())
      .catch(error => manageError(error, res))
  },
  getById: (req, res) => {
    return productUseCase.getProduct({ id: req.params.productId })
      .then(product => checkModelNone(product, res))
      .then(product => res.status(200).send({ data: product }))
      .catch(error => manageError(error, res))
  },
  deleteById: (req, res) => {
    return productUseCase.getProduct({ id: req.params.productId })
      .then(product => checkModelNone(product, res))
      .then(({ id }) => productUseCase.deleteProduct({ id }))
      .then(_ => res.sendStatus(204))
      .catch(error => manageError(error, res))
  }
})

module.exports = handlers

const createProductFromRequest = (request) => ({
  product: {
    kind: request.body.kind,
    name: request.body.name,
    description: request.body.description
  }
})
