const Product = require('../../../infraestructure/db/product/schema.js')
const productRepository = require('../../../infraestructure/db/product/repository.js')({ Product })
const productUseCase = require('../../../domain/product/usecase.js')({ productRepository })

module.exports = productUseCase
