const ShoppingCart = require('../../../infraestructure/db/shoppingcart/schema.js')
const Product = require('../../../infraestructure/db/product/schema.js')
const productRepository = require('../../../infraestructure/db/product/repository.js')({ Product })
const shoppingCartRepository = require('../../../infraestructure/db/shoppingcart/repository.js')({ ShoppingCart })
const shoppingCartService = require('./../../../infraestructure/service/shoppingcart.js')
const shoppingCartUseCase = require('../../../domain/shoppingcart/usecase.js')({ shoppingCartRepository, productRepository, shoppingCartService })

module.exports = shoppingCartUseCase
