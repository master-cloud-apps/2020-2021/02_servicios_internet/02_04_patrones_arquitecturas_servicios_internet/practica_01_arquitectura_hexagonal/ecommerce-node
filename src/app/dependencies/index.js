const routers = require('../routers/index.js')
const productUseCase = require('./usecases/product.js')
const shoppingCartUseCase = require('./usecases/shoppingcart.js')

module.exports = {
  productRouter: routers.productRouter({ productUseCase }),
  shoppingCartRouter: routers.shoppingCartRouter({ shoppingCartUseCase })
}
